## [1.0.3](https://gitlab.com/nath7098/personal-website/compare/v1.0.2...v1.0.3) (2024-08-26)

### Bug Fixes

* **eslint:** migrated from eslintrc to flat file config + fixed lints ([74cf046](https://gitlab.com/nath7098/personal-website/commit/74cf0469e96f76a8670527d11ef3a4c5ff05ee1d))

### Chores

* **deps:** setting pnpm as package manager ([fcdd5e6](https://gitlab.com/nath7098/personal-website/commit/fcdd5e6baba57f9f9e98458bab5f13036ab19c33))
* **deps:** setup renovate to avoid dependencies not up-to-date ([edb6e44](https://gitlab.com/nath7098/personal-website/commit/edb6e4476c8129d646be38ad2e09b9836fd28458))
* **deps:** upgrade major dependencies ([c810dc5](https://gitlab.com/nath7098/personal-website/commit/c810dc5dbdb6510c1919299f1b5d64b78e555fb9))
* **deps:** upgrade minor and patch dependencies ([206b208](https://gitlab.com/nath7098/personal-website/commit/206b2089068875c3d72733a375693ca3e639ae74))

## [1.0.2](https://gitlab.com/nath7098/personal-website/compare/v1.0.1...v1.0.2) (2024-06-21)

### Bug Fixes

* **conventional-changelog:** upgrade to conventional-changelog-conventionalcommits@8 ([9a152fe](https://gitlab.com/nath7098/personal-website/commit/9a152fee970b7fd5bf25b15111b64e80e1dd5b99))
* **npm:** fix vulnerabilities ([2a61b27](https://gitlab.com/nath7098/personal-website/commit/2a61b271663e12cf114c26a2bad1878863e37d69))
* **readme:** add infos ([883858e](https://gitlab.com/nath7098/personal-website/commit/883858e813d4c57ab302f13ac162ee18fe44127b))
* **readme:** fixed typo in readme ([306e54f](https://gitlab.com/nath7098/personal-website/commit/306e54f642d32871cd01a5358e0936743dafaef0))

## [1.0.1](https://gitlab.com/nath7098/personal-website/compare/v1.0.0...v1.0.1) (2024-05-14)


### Documentation

* change readme ([0419709](https://gitlab.com/nath7098/personal-website/commit/0419709bf3b52c894d8dfc95e3332875b45daef8))


### Chores

* added gitlab actions to issues templates ([46ff00e](https://gitlab.com/nath7098/personal-website/commit/46ff00ed74a0e04d97da82ada9b0fc08dd52d77b))
* adding template for issues and MR ([1f5eca5](https://gitlab.com/nath7098/personal-website/commit/1f5eca529bf7af96fe608a3dd27b3730555304cd))
* fixed issues templates ([735bc6a](https://gitlab.com/nath7098/personal-website/commit/735bc6ab5e06cfb720b4cce3b6bef1d319f36de2))
* rename issu_templates directory ([169ff9c](https://gitlab.com/nath7098/personal-website/commit/169ff9c43ab831dd3ea7f58c534cf00cc96bdcbe))

## 1.0.0 (2024-04-11)


### Features

* add merge request template ([107afe4](https://gitlab.com/nath7098/personal-website/commit/107afe4de344403d2d936cc31bc7b780bb43256c))
* **index:** meta ([002f5cb](https://gitlab.com/nath7098/personal-website/commit/002f5cbc4d007b3f6069f8b38333fc2089d3a8f3))


### Bug Fixes

* add required needs and dependencies in cicd ([7cb49f1](https://gitlab.com/nath7098/personal-website/commit/7cb49f1a2fde1c16f1737ae4e18ce0303081e07e))
* add required needs and dependencies in cicd ([ada7f6c](https://gitlab.com/nath7098/personal-website/commit/ada7f6c507600f1fb4ea75af31723a306b545daa))
* **cicd:** on check si deploy ok avec curl ([8588de6](https://gitlab.com/nath7098/personal-website/commit/8588de6fec1bdf1cf24bbe0dea829aaba95ac757))
* **ci:** change PROD_REF to master to allow deploy ([5f32571](https://gitlab.com/nath7098/personal-website/commit/5f325718628d5ec174c44fefe3fc4bbfb7208ff3))
* **ci:** use $WEB_URL to check container up ([25d1f93](https://gitlab.com/nath7098/personal-website/commit/25d1f930bc350ea9ed58055f959f8e7f005f6222))
* **deps:** update dependencies to fix CVE ([a8072ca](https://gitlab.com/nath7098/personal-website/commit/a8072cae77bad3071ba6fffea4f40bc78ed6eb83))
* disable click on tags without content ([6be7cee](https://gitlab.com/nath7098/personal-website/commit/6be7cee71d098492acd4f32e392d33e706810871))
* **docker:** missing semicolon after deamon off + change stats for logs ([6cd8ef1](https://gitlab.com/nath7098/personal-website/commit/6cd8ef1015d8ddc177e3f9cb0c9aa8d3a765ffdc))
* **docker:** move image name to docker run end since it's must be the last arg ([d757aa8](https://gitlab.com/nath7098/personal-website/commit/d757aa8c98e51cadf908cfe05dad01ac6423eb53))
* **docker:** remove trailing 'g' + add container run checking ([4291317](https://gitlab.com/nath7098/personal-website/commit/4291317ef7ca676e4da3e92d340fbe7c467ead7e))
* **favicon:** change favicon ([263278c](https://gitlab.com/nath7098/personal-website/commit/263278c1768c68792d8edbe2d109e735afd9cbe0))
* **ip:** fixed ip vulnerability from 2.0.0 to 2.0.1 ([f6b2803](https://gitlab.com/nath7098/personal-website/commit/f6b28037ac0324e5321c99326db6cad3bc2f9662))
* **lint:** add eslint basic configuration and fix issues ([0fb2702](https://gitlab.com/nath7098/personal-website/commit/0fb2702c237040b2780ee9420c73ccda9530561f))
* **lint:** add eslint basic configuration and fix issues ([6186a7f](https://gitlab.com/nath7098/personal-website/commit/6186a7f878bd9c7e29e8554f6cbcfa06673ffb17))
* **lint:** add eslint basic configuration and fix issues ([ebea937](https://gitlab.com/nath7098/personal-website/commit/ebea937370c8cd47f0a3dc38293509fa72d9b8db))
* **meta:** twitter card ([2838171](https://gitlab.com/nath7098/personal-website/commit/2838171e3406792a740f595d517a988c83835ccb))
* **projects:** alignment ([ac991c8](https://gitlab.com/nath7098/personal-website/commit/ac991c8d7d018ea1062cee53c57e44f442924f6e))
* **projects:** Fix links and text ([71c9baa](https://gitlab.com/nath7098/personal-website/commit/71c9baad1498c08c0a7e754cbf829d2c702a9d40))
* remove unused icons + add favicon ([433b5ea](https://gitlab.com/nath7098/personal-website/commit/433b5eaeb2557384b79d8c2821f53e14d8921c03))
* **sitemap:** date format ([4079d9b](https://gitlab.com/nath7098/personal-website/commit/4079d9bc05310de45af58b4761a701abc4994a35))
* **spotify:** api data structure ([1bdaafe](https://gitlab.com/nath7098/personal-website/commit/1bdaafe38b5d53e120a1b5bec7111a6c19daf062))


### Chores

* **deps:** update dependencies ([dcc8392](https://gitlab.com/nath7098/personal-website/commit/dcc8392ea3b25ea13c57ba1ced47756b3bc4eb4f))
* **deps:** upgrade dependencies patch ([5e29859](https://gitlab.com/nath7098/personal-website/commit/5e29859068208593f163ffb066aecc56c5ba4918))
* fix audit ([f457f35](https://gitlab.com/nath7098/personal-website/commit/f457f35284680eff8fb5b55b506e9f8d81b7c938))
* fix lock ([9106b95](https://gitlab.com/nath7098/personal-website/commit/9106b954a8302ef69e82556fb3c46a5c3c98204f))
* fix package.json ([38177cb](https://gitlab.com/nath7098/personal-website/commit/38177cb7431aa30e2910ce6adea4c98af0ceb9e4))
* minor dependencies update ([420f3fa](https://gitlab.com/nath7098/personal-website/commit/420f3fa1b7abd5cef710415410f1d45f84af5a13))
* patch dependencies update ([676c0ce](https://gitlab.com/nath7098/personal-website/commit/676c0ce838b0e0859fe77c4e3ea04d1bda180936))
* temporary fix for craft's releaserc generation - fixed in craft:v1.0.0-beta3 ([0a37638](https://gitlab.com/nath7098/personal-website/commit/0a376389cdae4d9d206d23a6578880b9693d06cc))
