<!-- This file is safe to edit. Once it exists it will not be overwritten. -->

# perso <!-- omit in toc -->

<p align="center">
  <img alt="Website" src="https://img.shields.io/website?url=https%3A%2F%2Fnathancouton.fr&style=for-the-badge">
  <img alt="GitLab Release" src="https://img.shields.io/gitlab/v/release/nath7098%2Fpersonal-website?gitlab_url=https%3A%2F%2Fgitlab.com&include_prereleases&sort=semver&style=for-the-badge">
  <img alt="GitLab Issues" src="https://img.shields.io/gitlab/issues/open/nath7098%2Fpersonal-website?gitlab_url=https%3A%2F%2Fgitlab.com&style=for-the-badge">
  <img alt="GitLab License" src="https://img.shields.io/gitlab/license/nath7098%2Fpersonal-website?gitlab_url=https%3A%2F%2Fgitlab.com&style=for-the-badge">
</p>

---
# __Personal Website__

This website is my personal portfolio developed with Vue 3. It is a migration from my old portfolio https://ancien-fr.nathancouton.fr made with html/css/js.

### __*Special thanks*__

 [Kilian Paquier](https://github.com/kilianpaquier) - Helped me enhancing the CI/CD, release process and versioning.

## __*Stack*__

|<img src="https://upload.wikimedia.org/wikipedia/commons/9/95/Vue.js_Logo_2.svg" width="128" />|<img src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Pinialogo.svg" height="128" />|<img src="https://upload.wikimedia.org/wikipedia/commons/f/f1/Vitejs-logo.svg" height="128" />|
|---|---|---|
|<img alt="Dynamic JSON Badge" src="https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fnath7098%2Fpersonal-website%2F-%2Fraw%2Fmaster%2Fpackage.json%3Fref_type%3Dheads&query=%24.dependencies.vue&style=for-the-badge&logoColor=transparent&label=%20&color=rgba(0%2C%200%20%2C0%2C%200.0001)">|<img alt="Dynamic JSON Badge" src="https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fnath7098%2Fpersonal-website%2F-%2Fraw%2Fmaster%2Fpackage.json%3Fref_type%3Dheads&query=%24.dependencies.pinia&style=for-the-badge&logoColor=transparent&label=%20&color=rgba(0%2C%200%20%2C0%2C%200.0001)">|<img alt="Dynamic JSON Badge" src="https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fnath7098%2Fpersonal-website%2F-%2Fraw%2Fmaster%2Fpackage.json%3Fref_type%3Dheads&query=%24.devDependencies.vite&style=for-the-badge&logoColor=transparent&label=%20&color=rgba(0%2C%200%20%2C0%2C%200.0001)">|


Also this project is now powered by [craft](https://github.com/kilianpaquier/craft), a project layout generator developed and maintained by Kilian Paquier 


## __*Back end*__

https://gitlab.com/nath7098/personal-api

I developed an api using Express.js. For instance it is used to connect and fetch Spotify api's data.

## __*CI/CD*__

- `development` : Pipeline builds and tests the code 
- `staging` : Pipeline builds and tests the code and ajob is created to manually deploy to [the staging environment](https://vps.nathancouton.fr)
- `master` : has the same pipelin as staging but includes an additional job for publishing (creating tag and release). The deploy job deploys to [production](https://nathancouton.fr)

  ## __*Deploy*__

 A Docker image is created with a Dockerfile. It is built and sent over SSH to my VPS before being started. Then there is a shell script used to check whether the container is running or not.

  ## __*Nginx*__

  On my VPS I have an Nginx reverse proxy docker container that intercepts the requests and dispatches them to the container listening on the right port and allows me to create subdomains. 

---

# Admin webapp under development

