import { createApp } from "vue";
import { createPinia } from "pinia";
import { createVfm } from "vue-final-modal";
import { createI18n } from "vue-i18n";

import "./assets/style/main.scss";
import "vue-final-modal/style.css";
import "vue3-toastify/dist/index.css";

import App from "./App.vue";
import router from "./router";
import fr from "@/assets/i18n/fr.js";
import en from "@/assets/i18n/en.js";
import NcContainer from "@/components/NcContainer.vue";

import vue3Spinner from "vue3-spinner";
import VueClipboard from "vue3-clipboard";
import Vue3Toasity from "vue3-toastify";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faSun,
  faMoon,
  faMobileScreen,
  faEnvelope,
  faSun as fasSun,
} from "@fortawesome/free-solid-svg-icons";
import { faSun as farSun } from "@fortawesome/free-regular-svg-icons";
import {
  faVuejs,
  faReact,
  faJava,
  faSquareJs,
  faSass,
  faAngular,
  faBootstrap,
  faHtml5,
  faNodeJs,
  faLinkedin,
  faGithub,
  faGitlab,
} from "@fortawesome/free-brands-svg-icons";

library.add(
  faVuejs,
  faReact,
  faJava,
  faSquareJs,
  faSass,
  faAngular,
  faBootstrap,
  faHtml5,
  faNodeJs,
  faLinkedin,
  faGithub,
  faGitlab,
  fasSun,
  farSun,
  faMoon,
  faMobileScreen,
  faEnvelope
);

const i18n = createI18n({
  legacy: false,
  locale: "fr",
  availableLocales: ["fr", "en"],
  fallbackLocale: "en",
  messages: {
    fr: fr,
    en: en,
  },
});

const app = createApp(App);

app
  .use(createPinia())
  .use(router)
  .use(i18n)
  .use(createVfm())
  .use(vue3Spinner)
  .use(VueClipboard, {
    autoSetContainer: true,
    appendToBody: true,
    closeButton: false,
  })
  .use(Vue3Toasity, {
    autoClose: 3000,
    // position: "top-right",
  })
  .component("nc-container", NcContainer)
  .component("font-awesome-icon", FontAwesomeIcon)
  .mount("#app");
