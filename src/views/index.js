import HomeView from "./HomeView.vue";
import AboutView from "./AboutView.vue";
import ExperienceView from "./ExperienceView.vue";
import SkillsView from "./SkillsView.vue";
import EducationView from "./EducationView.vue";
import ProjectsView from "./ProjectsView.vue";
import ContactView from "./ContactView.vue";
import BlogView from "./BlogView.vue";

export {
  HomeView,
  AboutView,
  ExperienceView,
  SkillsView,
  EducationView,
  ProjectsView,
  ContactView,
  BlogView,
};
