import { ref } from "vue";
import { defineStore } from "pinia";

export const useAppStore = defineStore("app", () => {
  const infos = ref({});
  const education = ref([]);
  const experience = ref([]);
  const project = ref({ projects: [], other: [] });
  const skills = ref({});
  const darkMode = ref(true);
  const spotifyData = ref({});
  const gamesData = ref({});
  const hobbiesData = ref({});
  const locale = ref("");
  const hasProcessLoading = ref(false);

  return {
    infos,
    darkMode,
    spotifyData,
    gamesData,
    hobbiesData,
    locale,
    hasProcessLoading,
  };
});
