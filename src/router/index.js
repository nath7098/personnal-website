import { createRouter, createWebHistory } from "vue-router";
import { routes } from "@/assets/helper/helper.js";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return { top: 0 };
  },
});

router.afterEach((to, from) => {
  const toIndex = routes.findIndex((r) => r.name === to.name);
  const fromIndex = routes.findIndex((r) => r.name === from.name);
  let transition = "fade";
  if (toIndex < fromIndex) {
    transition = "slide-right";
  } else if (toIndex > fromIndex) {
    transition = "slide-left";
  }
  to.meta.transition = transition;
});

export default router;
