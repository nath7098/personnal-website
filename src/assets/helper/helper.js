import {
  HomeView,
  AboutView,
  ExperienceView,
  SkillsView,
  EducationView,
  ProjectsView,
  ContactView,
  BlogView,
} from "@/views";

export const routes = [
  {
    path: "/",
    name: "Home",
    component: HomeView,
  },
  {
    path: "/about",
    name: "About",
    component: AboutView,
  },
  {
    path: "/experience",
    name: "Experience",
    component: ExperienceView,
  },
  {
    path: "/skills",
    name: "Skills",
    component: SkillsView,
  },
  {
    path: "/education",
    name: "Education",
    component: EducationView,
  },
  {
    path: "/projects",
    name: "Projects",
    component: ProjectsView,
  },
  {
    path: "/contact",
    name: "Contact",
    component: ContactView,
  },
];
