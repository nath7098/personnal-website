export default {
  navigation: {
    home: "Accueil",
    about: "A propos",
    experience: "Expériences",
    skills: "Compétences",
    education: "Education",
    projects: "Projets",
    contact: "Contact",
    resume: "CV",
  },
  home: {
    greetings: "Hello! Je m'appelle",
    position: "Développeur Fullstack",
    button: "En savoir plus",
  },
  about: {
    description: "Curieux - Esprit d'équipe",
    pickupline: "J'aime les challenges",
    musics: "Mon top spotify",
    games: "Mes jeux",
    hobbies: {
      title: "Mes hobbies",
      music: "Musique",
      australia: "Australie",
    },
  },
  experience: [
    {
      date: "2021 - Aujourd'hui",
      title: "ACII by Audensiel",
      content: "Développeur Fullstack Java Vuejs",
      options: {
        details: [
          {
            date: "2021 - Aujourd'hui",
            title: "Mutuelle de Poitiers Assurances",
            content: "Développement d'application web",
            skills: [
              {
                label: "Vue 2",
                class: "vue",
                details:
                  "Nous utilisons Vuejs 2 avec VueX et des librairies externes comme Vue Property Decorator, Vue Class Component et Vuex Property Decorator",
              },
              {
                label: "Java 8",
                class: "java",
                details:
                  "Nous utilisons Java EE 8 Pour le backend avec Spring Boot et Hibernate",
              },
              {
                label: "Spring",
                class: "spring",
                details:
                  "Nous utilisons Spring Boot 2 et Spring Security pour construire les APIs",
              },
              {
                label: "Agile",
                class: "agile",
                details:
                  "Le projet a été organisé en sprints de 3 semaines avec une réunion hebdomadaire et des revues de code",
              },
            ],
          },
          {
            date: "2021",
            title: "Tempo",
            content: "Développement d'application web",
            skills: [
              {
                label: "Angular 11",
                class: "angular",
                details:
                  "Nous avons utilisé Angular 11 en frontend. Le but était de monter en compétences sur une version plus récente.",
              },
              {
                label: "Java 15",
                class: "java",
                details:
                  "Nous avons utilisé Java 15 avec Spring Boot et Hibernate.",
              },
              {
                label: "Spring",
                class: "spring",
                details:
                  "Nous avons utilisé Spring Boot, MVC and Security pour construire l'API.",
              },
              {
                label: "Agile",
                class: "agile",
                details:
                  "Etant un projet réservé aux inter-mission, il y avait un grand turn-over. Nous avons donc du documenter le plus possible le projet et écrire des tests fonctionnels et end to end pour s'assurer de la continuité et faciliter la prise en main du projet. Nous avions des daily, des revues de code et des merge requests sur des sprints d'une semaine.",
              },
            ],
          },
        ],
      },
    },
    {
      date: "2020",
      title: "Sopra Steria",
      content: "Stage de développeur Java Angular",
      options: {
        details: [
          {
            content:
              "Migration Angular 2 et développement d'évolutions et correctifs sur des applications de gestions des professionnels de santé et des dossiers médicaux",
            skills: [
              {
                label: "Angular",
                class: "angular",
                details:
                  "Nous avons travaillé sur la migration d'une application de Flex vers Angular 2",
              },
              {
                label: "Java",
                class: "java",
                details: "Nous avons gardé le backend existant en Java 8",
              },
              {
                label: "Agile",
                class: "agile",
                details:
                  "Le projet a été réorganisé en agile avec des daily, des revues de code et des sprints de 3 semaines",
              },
            ],
          },
        ],
      },
    },
    {
      date: "2019",
      title: "Sopra Steria",
      content: "Stage de développeur Java Flex",
      options: {
        details: [
          {
            content:
              "Développement d'évolutions et correctifs sur des applications de gestions des professionnels de santé et des dossiers médicaux",
            skills: [
              {
                label: "Flex",
                class: "flex",
                details:
                  "Les applications étaient développées en Flex pour le frontend",
              },
              {
                label: "Java",
                class: "java",
                details: "Le backend était développé en Java 8",
              },
            ],
          },
        ],
      },
    },
  ],
  skills: {
    experience: "Expérience",
    exp: "Aucune | {n} an | {n} ans",
  },
  education: [
    {
      date: "2023",
      title: "Vuejs",
      content:
        "Formation de 3 jours pour enrichir et consolider mes connaissance sur Vue 2 et commencer à apprendre les nouveautés apportées par Vue 3",
    },
    {
      date: "2017 - 2020",
      title: "Polytech Tours",
      content: "Diplôme d'ingénieur informatique",
    },
    {
      date: "2015 - 2017",
      title: "IUT Angoulême",
      content: "DUT Génie Electronique et Informatique Industrielle",
    },
  ],
  projects: [
    {
      image: { alt: "Mutuelle de Poitiers Assurances" },
      title: "Prévoyance",
      description:
        "Ma mission à la mutuelle de Poitiers Assurance est de développer une application pour leur nouveau produit de Prévoyance.",
      tags: [
        { label: "Vue 2", class: "vue", details: "" },
        { label: "Ts", class: "ts", details: "" },
        { label: "VueX", class: "vuex", details: "" },
        { label: "Java", class: "java", details: "" },
      ],
    },
    {
      image: { alt: "Portfolio" },
      title: "Portfolio",
      description:
        "Migration de mon ancien portfolio static (html, css) vers Vue/ExpressJs",
      tags: [
        { label: "Vue 3", class: "vue", details: "" },
        { label: "Vite", class: "vite", details: "" },
        { label: "Pinia", class: "pinia", details: "" },
        { label: "ExpressJs", class: "express", details: "" },
      ],
    },
    {
      image: { alt: "Solveur de TSP" },
      title: "Application web de solveur de TSP",
      description:
        "Site web de solveur de TSP créé durant ma dernière année à Polytech Tours en projet libre. Son but est de faire découvrir de manière intéractive le problème du TSP et les méthodes pour le résoudre.",
      tags: [
        { label: "JQuery", class: "jquery", details: "" },
        { label: "p5", class: "p5", details: "" },
        { label: "Bootstrap", class: "bootstrap", details: "" },
      ],
    },
    {
      image: { alt: "Hololens RGBD Stream to point cloud" },
      title: "Hololens RGBD Stream en nuage de points",
      description:
        "Projet de fin d'étude à Polytech. Recherche et développement d'une application pour les Hololens visant à récupérer les données des capteurs de profondeur et d'images et à recréer un nuage de points en 3D en réalité augmentée.",
      tags: [
        { label: "Unity", class: "unity", details: "" },
        { label: "C#", class: "csharp", details: "" },
        { label: "C++", class: "cpp", details: "" },
      ],
    },
  ],
  other: [
    {
      image: { alt: "Virtual moneybox" },
      title: "Tirelire virtuelle",
      description:
        "Application web crée lors d'un projet de groupe de 4ème année à Polytech Tours avec 6 élèves. Le projet a été commandé par une équipe de Sopra Baking Software",
      tags: [
        { label: "Angular", class: "angular" },
        { label: "Java", class: "java" },
        { label: "Bootstrap", class: "bootstrap" },
        { label: "Agile", class: "agile" },
      ],
    },
    {
      image: { alt: "SwalloWin Sound" },
      title: "SwalloWin Sound",
      description:
        "Projet étudiant en groupe de 5 à Polytech Tours. Revfonte d'une application android visant à aider les médecin à détecter des pathologies en enregistrant les bruits de déglutitions.",
      tags: [
        { label: "Android", class: "vue" },
        { label: "Java", class: "java" },
      ],
    },
    {
      image: { alt: "Première version de mon site web" },
      title: "Mon premier site web",
      description:
        "Mon site web personnel développé en html/css/js sur mon temps libre à Polytech Tours afin d'apprendre les bases du développement web et de mettre en avant mes compétences.",
      tags: [
        { label: "Html", class: "html" },
        { label: "Css", class: "css" },
        { label: "Js", class: "js" },
        { label: "JQuery", class: "jquery" },
        { label: "Bootstrap", class: "bootstrap" },
      ],
    },
    {
      image: { alt: "AJL Peinture" },
      title: "AJL Peinture",
      description:
        "Un site web créé pour un artisan en 2020. Il a été développé avec React et firebase ce qui m'a permis d'apprendre à utiliser ce framework et monter en compétences dessus.",
      tags: [
        { label: "React", class: "react" },
        { label: "Firebase", class: "firebase" },
        { label: "Bootstrap", class: "bootstrap" },
      ],
    },
  ],
  contact: {
    title: "Contactez moi",
    mail_title: "Envoyez moi un e-mail",
    name: "Votre nom",
    mail: "Votre e-mail",
    message: "Votre message",
    send: "Envoyer",
    copy_phone: {
      ko: "Erreur: Impossible de copier le numéro. Vous pouvez le noter : 0646898223",
      ok: "Copié 😇 !",
    },
    mail_response: {
      ko: "Une erreur s'est produite 😞 Vous pouvez toujours me contacter directement par mail : contact@nathancouton.fr",
      ok: "Votre message a bien été envoyé 😃 Je vous répondrai dès que possible!",
    },
  },
};
