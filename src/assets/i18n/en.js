export default {
  navigation: {
    home: "Home",
    about: "About",
    experience: "Experience",
    skills: "Skills",
    education: "Education",
    projects: "Projects",
    contact: "Contact",
    resume: "Resume",
  },
  home: {
    greetings: "Hi! I'm",
    position: "Fullstack Developer",
    button: "Get to know me better",
  },
  about: {
    description: "Curious - Team-working",
    pickupline: "I love challenges",
    musics: "My spotify top list",
    games: "My games",
    hobbies: {
      title: "My hobbies",
      music: "Music",
      australia: "Australia",
    },
  },
  experience: [
    {
      date: "2021 - Today",
      title: "ACII by Audensiel",
      content: "Fullstack Java Vuejs developer",
      options: {
        details: [
          {
            date: "2021 - Today",
            title: "Mutuelle de Poitiers Assurances",
            content: "Web app development",
            skills: [
              {
                label: "Vue 2",
                class: "vue",
                details:
                  "We use Vuejs 2 with Options API and VueX coupled with some external libraries as Vue Property Decorator, Vue Class Component and Vuex Property Decorator",
              },
              {
                label: "Java 8",
                class: "java",
                details:
                  "We use Java EE 8 as our backend, coupled with Spring Boot and Hibernate",
              },
              {
                label: "Spring",
                class: "spring",
                details:
                  "We use Spring Boot 2 and Spring Security to build our apis",
              },
              {
                label: "Agile",
                class: "agile",
                details:
                  "The project was organized in sprints of 3 weeks with a weekly meeting and code reviews",
              },
            ],
          },
          {
            date: "2021",
            title: "Tempo",
            content: "Web app development",
            skills: [
              {
                label: "Angular 11",
                class: "angular",
                details:
                  "We used Angular 11 as our front-end. The goal was to familiarize with the most recent version at the time.",
              },
              {
                label: "Java 15",
                class: "java",
                details: "We used Java 15 coupled with Hibernate.",
              },
              {
                label: "Spring",
                class: "spring",
                details:
                  "We used Spring Boot, MVC and Security to build our api stack.",
              },
              {
                label: "Agile",
                class: "agile",
                details:
                  "The team had a strong turn-over due to the fact that it was a project we were working on between our missions. We needed to document it as much as possible for a fast start when entering the project. We had daily meetings, code review and merge requests and we worked in sprints of 1 week.",
              },
            ],
          },
        ],
      },
    },
    {
      date: "2020",
      title: "Sopra Steria",
      content: "Java Angular developer intern",
      options: {
        details: [
          {
            content:
              "Angular 2 migration & development of evolutions and fixes on applications for Health Professionals and Medical records management",
            skills: [
              {
                label: "Angular",
                class: "angular",
                details:
                  "We migrated the existing applications from Flex to Angular 2.",
              },
              {
                label: "Java",
                class: "java",
                details: "We used Java 8 as our back end.",
              },
              {
                label: "Agile",
                class: "agile",
                details:
                  "The project was reorganized in agile with daily meetings, code reviews and 3-weeks sprints.",
              },
            ],
          },
        ],
      },
    },
    {
      date: "2019",
      title: "Sopra Steria",
      content: "Java Flex developer intern",
      options: {
        details: [
          {
            content:
              "Development of evolutions and fixes on applications for Health Professionals and Medical records management",
            skills: [
              {
                label: "Flex",
                class: "flex",
                details:
                  "The applications were developed using Flex as front-end",
              },
              {
                label: "Java",
                class: "java",
                details: "The back end was developed using Java 8",
              },
            ],
          },
        ],
      },
    },
  ],
  skills: {
    experience: "Experience",
    exp: "none | {n} year | {n} years",
  },
  education: [
    {
      date: "2023",
      title: "Vuejs",
      content:
        "3-days formation to improve my skills on VueJs 2 and learn VueJs 3 new features",
    },
    {
      date: "2017 - 2020",
      title: "Polytech Tours",
      content: "Computer Science engineer degree",
    },
    {
      date: "2015 - 2017",
      title: "IUT Angoulême",
      content: "Electronics and Information Technology University degree",
    },
  ],
  projects: [
    {
      image: { alt: "Mutuelle de Poitiers Assurances" },
      title: "Prevoyance",
      description:
        "My mission at Mutuelle de Poitiers Assurance. We developed an app for their new Prevoyance insurance.",
      tags: [
        { label: "Vue 2", class: "vue", details: "" },
        { label: "Ts", class: "ts", details: "" },
        { label: "VueX", class: "vuex", details: "" },
        { label: "Java", class: "java", details: "" },
      ],
    },
    {
      image: { alt: "Portfolio" },
      title: "Portfolio",
      description:
        "My personal developer portfolio migration from html/css to Vue/ExpressJs",
      tags: [
        { label: "Vue 3", class: "vue", details: "" },
        { label: "Vite", class: "vite", details: "" },
        { label: "Pinia", class: "pinia", details: "" },
        { label: "ExpressJs", class: "express", details: "" },
      ],
    },
    {
      image: { alt: "TSP Solver" },
      title: "TSP Web Solver",
      description:
        "A TSP solver website created during my last year at Polytech Tours. It's purpose was to share an interactive way of discovering and learning the way the TSP can be solved.",
      tags: [
        { label: "JQuery", class: "jquery", details: "" },
        { label: "p5", class: "p5", details: "" },
        { label: "Bootstrap", class: "bootstrap", details: "" },
      ],
    },
    {
      image: { alt: "Hololens RGBD Stream to point cloud" },
      title: "Hololens RGBD Stream to point cloud",
      description:
        "Student project during 5th year at Polytech. Research & development of an app for the Hololens to acquire and recreate in 3D a points cloud using the depth sensor.",
      tags: [
        { label: "Unity", class: "unity", details: "" },
        { label: "C#", class: "csharp", details: "" },
        { label: "C++", class: "cpp", details: "" },
      ],
    },
  ],
  other: [
    {
      image: { alt: "Virtual moneybox" },
      title: "Virtual Moneybox",
      description:
        "A web app created during my 4th year at Polytech in a group of 6 students. This app was ordered by a team at Sopra Banking Software.",
      tags: [
        { label: "Angular", class: "angular" },
        { label: "Java", class: "java" },
        { label: "Bootstrap", class: "bootstrap" },
        { label: "Agile", class: "agile" },
      ],
    },
    {
      image: { alt: "SwalloWin Sound" },
      title: "SwalloWin Sound",
      description:
        "A students project in group. Reworking of an android app helping doctors to detect deceases using swallowing sounds.",
      tags: [
        { label: "Android", class: "vue" },
        { label: "Java", class: "java" },
      ],
    },
    {
      image: { alt: "Website first version" },
      title: "Website first version",
      description:
        "My personal developer portfolio created while I was in Polytech Tours to experience and learn about web development basics.",
      tags: [
        { label: "Html", class: "html" },
        { label: "Css", class: "css" },
        { label: "Js", class: "js" },
        { label: "JQuery", class: "jquery" },
        { label: "Bootstrap", class: "bootstrap" },
      ],
    },
    {
      image: { alt: "AJL Peinture" },
      title: "AJL Peinture",
      description:
        "A website I created for an artisan in 2020. I used React to build more knowledge about this framework.",
      tags: [
        { label: "React", class: "react" },
        { label: "Firebase", class: "firebase" },
        { label: "Bootstrap", class: "bootstrap" },
      ],
    },
  ],
  contact: {
    title: "Let's get in touch",
    mail_title: "Send me an e-mail",
    name: "Your name",
    email: "Your e-mail",
    message: "Your message",
    send: "Send",
    copy_phone: {
      ko: "Error copying the phone number to clipboard 😞 Here it is : 0646898223",
      ok: "Copied 😇 !",
    },
    mail_response: {
      ko: "Error sending an email 😞 Here is my address : contact@nathancouton.fr",
      ok: "Message sent 😃 I'll get back to your as soon as possible!",
    },
  },
};
