## 💥 Fatal Error Report

### Description
Describe the critical error that needs immediate attention.

### Impact
Explain the severity and impact of this error.

### Steps to Reproduce (if applicable)
1. Step 1
2. Step 2
3. Step 3
   ...

### Possible Solution (if any)
Share any insights or potential solutions you have.

### Additional Information
Add any other context or screenshots about the error here.

/label ~"Fatal" ~"Backlog"
/cc @nath7098