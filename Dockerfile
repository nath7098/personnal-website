#############################
#        STAGE BUILD        #
#############################
FROM node:lts-slim as build

ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

WORKDIR /app

COPY . .

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prefer-offline "${NODE_INSTALL_EXTRA_OPTS}" && \
    pnpm run build

#############################
#         STAGE RUN         #
#############################
FROM nginx:stable-alpine

LABEL org.opencontainers.image.authors="nath7098"
LABEL org.opencontainers.image.vendor="nath7098"

LABEL org.opencontainers.image.title="personal-website"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.url="gitlab.com/nath7098/personal-website"
LABEL org.opencontainers.image.source="gitlab.com/nath7098/personal-website"
LABEL org.opencontainers.image.documentation="gitlab.com/nath7098/personal-website"

WORKDIR /app

COPY --from=build /app/dist ./
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE ${EXPOSE_PORT}

CMD ["nginx", "-g", "daemon off;"]